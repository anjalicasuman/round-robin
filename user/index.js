const Router        = require('express').Router();
const validation    = require('./validations/validation');
const controller    = require('./controller/controller');
const rrcontroller  = require('./controller/rrcontroller');
const passport      = require('passport');

Router.post('/signup',validation.newUser,validation.putUser, validation.sendMail);
Router.get('/auth/google', passport.authenticate('google', { scope: ['profile','email'] }));
Router.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/',session: false }),
  function(req, res) {
    console.log("redirected-google");
    res.redirect('/profile');
  });
Router.post('/login',validation.getUser, validation.checkUser, validation.createToken);
Router.post('/create_task',controller.verifyToken, controller.createTask);
Router.get('/autoassign',rrcontroller.assignTask);
Router.get('/response',rrcontroller.result);
Router.put('/update',controller.verifyToken, controller.updateUser);
module.exports = Router;