const Promise           = require('bluebird');
const Joi               = require('joi');
const constant          = require('../../properties/constants');
const jwt               = require('../../utilities/jwtToken');
const sql               = require('../../services/user');
const task              = require('../../services/task');
const schema            = require('../../utilities/joiSchema');
const bcrypt            = require('../../utilities/bcryptHash');
const request           = require('request-promise');

var timerId = 0;

function verifyToken(req,res,next)
{
    const bearerHeader =  req.headers['authorization']; //get auth header value
    if(typeof bearerHeader !== 'undefined')            // check if auth undefined
    {
        
        const key = 'secretcustomerkey';
        const bearer = bearerHeader.split(' ');
        const token = bearer[1];
        Joi.validate({access_token :token}, schema.Token, function (err, value)  //validating the entered information
        {
            if(err)
            {
                res.json({                               // providing error message in json format
                message: 'Insufficient information was supplied. Please check and try again.',
                status: constant.responseFlags.INSUFFICIENT_DATA,
                data: err.details[0].message.replace(/["]/ig, ''),
                });
            }
            else
            {
                Promise.coroutine(function*()
                {
                    return yield jwt.check_jwt(token,key);
                })().then((result)=>
                {
                    req.id = result.id;
                    next();
                },(error)=>
                {
                    res.json({
                        //forbidden
                        status  :  constant.responseFlags.INVALID_ACCESS,
                        message :  constant.responseMessages.INVALID_ACCESS_TOKEN,
                        data    :  {error}
                    });
                });
            }
        });         
    }
    else{
        //forbidden
        res.json({
            status  :  constant.responseFlags.FORBIDDEN,
            message :  constant.responseMessages.INVALID_ACCESS,
            data    : {}
        });
    }
}

function createTask(req,res)
{
    let newtask =
    {
        user_id         : req.id,
        from_long       : req.body.from_long,
        from_lat        : req.body.from_lat,
        to_long         : req.body.to_long,
        to_lat          : req.body.to_lat,
        status          : 0,
        autoassignment  : req.body.autoassign
    };
    Promise.coroutine(function*()
    {
        let value = yield task.create_task(newtask);
        newtask.task_id = value.insertId;
        if(newtask.autoassignment == 0)
        {
            res.json({                               
                message: constant.responseMessages.ACTION_COMPLETE,
                status : constant.responseFlags.ACTION_COMPLETE,
                data   : newtask
            });
        }
        else if(newtask.autoassignment == 1)
        {
            let options = 
            {
                method: 'GET', 
                url: 'http://localhost:3000/user/autoassign', 
                headers: 
                { 
                    'Postman-Token': '6fa22556-b7e8-44d8-a0e4-a8016503efb8',
                    'cache-control': 'no-cache',
                    'Content-Type' : 'application/json' 
                }, 
                body: newtask,
                json: true
            };
            let body = yield request(options);
            res.json({                               
                message: constant.responseMessages.ACTION_COMPLETE,
                status : constant.responseFlags.ACTION_COMPLETE,
                data   : body
            });
        }
        else
        {
            throw new Error("WRONG INPUT ID.");
        }
    })().catch((error)=>
    {
        res.json({
            status  :  constant.responseFlags.OPERATION_UNSUCESSFUL,
            message :  constant.responseMessages.OPERATION_UNSUCESSFUL,
            data    :  {error}
        });
    });
}
function updateUser(req,res)
{
    let user =
    {
        user_id  : req.id,
        password : req.body.password
    };
    Promise.coroutine(function* ()
    {
        let value = yield sql.get_user(null,user.user_id);
        user.hashpass = value[0].password;
        let check = yield bcrypt.check_pass(user.password,user.hashpass);
        if(check == true)
        {
            user.new_pass = req.body.new_password;
            let hashpass = yield bcrypt.encode_pass(user.new_pass);
            let value = yield sql.up_pass(hashpass,user.user_id);
            res.json({                               
                message: constant.responseMessages.ACTION_COMPLETE,
                status : constant.responseFlags.ACTION_COMPLETE,
                data   : value
            });        
        } 
        else
        {
            res.json({
                //forbidden
                status  :  constant.responseFlags.INVALID_ACCESS,
                message :  constant.responseMessages.INVALID_PASSWORD,
                data    :  {error}
            });
        }
    })().catch((error)=>
    {
        res.json({
            //forbidden
            status  :  constant.responseFlags.INVALID_ACCESS,
            message :  constant.responseMessages.OPERATION_UNSUCESSFUL,
            data    :  {error}
        });
    });
}



module.exports =
{
    verifyToken : verifyToken,
    createTask  : createTask,
    updateUser  : updateUser
}





