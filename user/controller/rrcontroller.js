const autoassignTask    = require('../../utilities/autoassignment');
const email             = require('../../services/email_table');
const CronJob           = require('cron').CronJob;
const nodemailer        = require('nodemailer');
const config            = require('../../config/config')

async function assignTask(req,res)
{
    try
    {
        let task = req.body;
        let distant = await autoassignTask.fetch_distance(task.task_id);
        const near5 = distant.filter(function (value)
        {
            return value.distance < 5;
        });
        for(let i =0;i<near5.length;i++)
        {
            await email.enter_values(task.task_id,near5[i].driver_id);
        }      
        let transporter = nodemailer.createTransport(config.Transport);
        var add = (function () {
            var counter = -1;
            return function () {counter += 1; return counter}
          })();
        
        // -----------------------------------------------------------------------------------
        // let i = add();
        // await calljob(transporter,i,task,near5);
        // timerId = setInterval(async function()
        // {         
        //     let i = add();
        //     await email.time_up(task.task_id, near5[i-1].driver_id);
        //     let value = await email.check_values(task.task_id, near5[i-1].driver_id);
        //     if(value[0].response == 1)
        //     {
        //         clearInterval(timerId); 
        //         console.log('stop as driver assigned');
        //         res.json({
        //             status : constant.responseFlags.ACTION_COMPLETE,
        //             message : constant.responseMessages.ACTION_COMPLETE,
        //             data : { Message : "Driver has been assigned." }
        //         });
        //     }
        //     else
        //     {
        //         if(i == near5.length)
        //         {
        //             clearInterval(timerId); 
        //             console.log('stop as could not be assigned.');
        //             res.json({
        //                 status : constant.responseFlags.OPERATION_UNSUCESSFUL,
        //                 message : constant.responseMessages.OPERATION_UNSUCESSFUL,
        //                 data : { Message : "Driver could not be assigned." }
        //             });
        //         }
        //         else
        //         {
        //             await calljob(transporter,i,task,near5);
        //         }
        //     }
        // },20000);
        // --------------------------------------------------------------------------------------------
        // new CronJob('0,30 * * * * *', async function() {
        //    let i = add();
        //    let value = await email.check_values(task.task_id, near5[i-1].driver_id);
        //    if(value[0].response == 1)
        //    {
        //        clearInterval(timerId); 
        //        console.log('stop as driver assigned');
        //        res.json({
        //            status : constant.responseFlags.ACTION_COMPLETE,
        //            message : constant.responseMessages.ACTION_COMPLETE,
        //            data : { Message : "Driver has been assigned." }
        //        });
        //    }
        //    else
        //    {
        //        if(i == near5.length)
        //        {
        //            clearInterval(timerId); 
        //            console.log('stop as could not be assigned.');
        //            res.json({
        //                status : constant.responseFlags.OPERATION_UNSUCESSFUL,
        //                message : constant.responseMessages.OPERATION_UNSUCESSFUL,
        //                data : { Message : "Driver could not be assigned." }
        //            });
        //        }
        //        else
        //        {
        //            await calljob(transporter,i,task,near5);
        //        }
        //   });
        // ------------------------------------------------------------------------------------
    }
    catch(error)
    {
        console.log(error);
        let err = new Error("Could not autoassign the driver.");
        res.json({
            status  :  constant.responseFlags.OPERATION_UNSUCESSFUL,
            message :  constant.responseMessages.OPERATION_UNSUCESSFUL,
            data    :  {err}
        });
    }
}

async function result(req,res)
{
    try
    {
        let task_id = req.query.task_id;
        let driver_id = req.query.driver_id;
        let flag = req.query.flag;
        let value = await email.get_time(task_id,driver_id);
        if(value[0].time_up == 1)
        {
            res.send("SORRY YOUR TIME IS UP.");
        }
        else
        {
            await email.up_values(task_id,driver_id,flag);
            if(flag ==1)
            {
                res.send("YOU HAVE ACCEPTED THE JOB.");
            }
            else
            {
                res.send("YOU HAVE REJECTED THE JOB.");
            }
        }
        
    }
    catch(error)
    {
        res.json({
            status : constant.responseFlags.OPERATION_UNSUCESSFUL,
            message: constant.responseMessages.OPERATION_UNSUCESSFUL,
            data : error
        })
    }
    
}

module.exports =
{
    assignTask  : assignTask,
    result      : result
}

async function calljob(transporter,i,task,near5)
{
    console.log("i->",i);
    let text = " Accept --> http://ec956248.ngrok.io/user/response?task_id="+task.task_id+"&driver_id="+near5[i].driver_id+"&flag=1"+"\n Reject --> http://ec956248.ngrok.io/user/response?task_id="+task.task_id+"&driver_id="+near5[i].driver_id+"&flag=1";
    let mailOptions = {
        from: 'anjalica.test@gmail.com', 
        to: near5[i].email, 
        subject: "JOB REQUEST",
        text: text, 
    };      
    let info = await transporter.sendMail(mailOptions);
    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}

function nextDriver(task_id,driver,timerId)
{
    let timerId = setTimeout( async function()
    {
        let i = add();
        await email.time_up(task.task_id, near5[i-1].driver_id);
        let value = await email.check_values(task.task_id, near5[i-1].driver_id);
        if(value[0].response == 1)
        {
            clearTimeout(timerId); 
            console.log('stop as driver assigned');
            res.json({
                status : constant.responseFlags.ACTION_COMPLETE,
                message : constant.responseMessages.ACTION_COMPLETE,
                data : { Message : "Driver has been assigned." }
            });
        }
        else
        {
            if(i == near5.length)
            {
                clearTimeout(timerId); 
                console.log('stop as could not be assigned.');
                res.json({
                    status : constant.responseFlags.OPERATION_UNSUCESSFUL,
                    message : constant.responseMessages.OPERATION_UNSUCESSFUL,
                    data : { Message : "Driver could not be assigned." }
                });
            }
            else
            {
                await calljob(transporter,i,task,near5);
            }
        }       
    },20000)
}


// function autoassign(){
//     //case jobid drivers in db already 
//     //driver distange  range 
//     // email ids associated drivers email trigger
//     //db insert rows email sent 0 and status empty
//     //send email one  
//     //as soon as driver accepts function return
//     //setimieout cron job
//     //if reject 30 sec then continue this process 
//     //10 sec if no response that means continue this process mark status rejected for this driver 
// }



// function jobstatusUpdate(){
//   //driverid , jobid 
//   //status condition
//   //update table and set the status accordingly
// }
//     //if reject then continue this process 
//     //if reject then continue this process 



//     JOBS    DRIVER    emailsent    status 
//     job-id  driver1    1              accept          
//     //      driver2    0
