const Promise       = require('bluebird');
const nodemailer    = require('nodemailer');
const Joi           = require('joi');
const usermodel     = require('../../services/usermodel');
const schema        = require('../../utilities/joiSchema');
const sql           = require('../../services/user');
const bcrypt        = require('../../utilities/bcryptHash');
const constant      = require('../../properties/constants');
const jwt           = require('../../utilities/jwtToken')


function newUser(req, res, next)
{
    let user = new usermodel(req.body);
    Joi.validate(user, schema.Schema, function (err, value)  //validating the entered information
    {
        if(err)
        {
            res.json({                               // providing error message in json format
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : err.details[0].message.replace(/["]/ig, '')
            });
        }
        else
        {    
            req.user = user;
            next();
        }
    });
}
function putUser(req,res,next)
{
    Promise.coroutine(function* ()
    {
        let user = req.user;
        user.password = yield bcrypt.encode_pass(user.password);
        let value = yield sql.enter_user(user);
        next();
    })().catch((error)=>
    {
        console.log(error);
        res.json({                               
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : {error}
        });
    });
}
async function sendMail(req,res,next)
{
    try
    {
        let user = req.user;
        let transporter = nodemailer.createTransport({
            service : 'gmail',
            auth: {
            user: "anjalica.test@gmail.com", // generated ethereal user
            pass: "Password@123" // generated ethereal password
            }
        });
        
        // setup email data with unicode symbols
        let mailOptions = {
            from: 'anjalica.test@gmail.com', // sender address
            to: user.email, // list of receivers
            subject: "Hello ✔", // Subject line
            text: "this message is from nodemailer you have signed up", // plain text body
        };
        
        let info = await transporter.sendMail(mailOptions);
        
        console.log("Message sent: %s", info.messageId);
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        res.json({
            message: constant.responseMessages.SUCESSFULLY_SIGNED_UP,
            status : constant.responseFlags.SUCCESSFUL_SIGNED_UP,
            data: {
                Firstname : user.first_name,
                Lastname  : user.last_name,
                Email     : user.email,
                DOB       : user.DOB,  
                Longitude : user.current_long,
                Latitude  : user.current_lat
            }
        });
    }
    catch(e)
    {
        console.error(e);
    }
}

function getUser(req,res,next)
{
    let user = 
    {
        email    : req.body.email,
        password : req.body.password 
    }
    Joi.validate(user, schema.Schema, function (err, info)  //validating the entered information
    {
        if(err)
        {
            res.json({                               // providing error message in json format
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : err.details[0].message.replace(/["]/ig, '')
            });
        }
        else
        {
            Promise.coroutine(function*()
            {
                return yield sql.get_user(user.email,null);
            })().then((value)=>
            {
                if(value[0] == undefined)
                {
                    res.json({
                        message: constant.responseMessages.INVALID_EMAIL_ID,
                        status : constant.responseFlags.INVALID_EMAIL_ID,
                        data   : {}
                    });
                }
                else
                {   
                    user.hashpass = value[0].password;
                    user.id       = value[0].user_id;
                    req.user = user;
                    next();
                }
            },(error)=>
            {
                console.log(error);
                res.json({                               
                    message: constant.responseMessages.INSUFFICIENT_DATA,
                    status : constant.responseFlags.INSUFFICIENT_DATA,
                    data   : {error}
                });
            });
        }
    });    
}
function checkUser (req,res,next)
{
    user = req.user; 
    Promise.coroutine(function*()
    {
        return yield bcrypt.check_pass(user.password,user.hashpass);
    })().then((value)=>
    {
      if(value == true)
      {
        req.payload = {
            id : user.id
        };
        next();
      } 
      else
      {
        res.json({
            message: constant.responseMessages.INVALID_PASSWORD,
            status : constant.responseFlags.INVALID_PASSWORD,
            data   : {}
        });
      } 
    },(error)=>{
        res.json({                               
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : {error}
        });
    });    
}

function createToken (req,res) 
{
    let payload = req.payload,
        key = 'secretcustomerkey';
    Promise.coroutine(function*()
    {
        return yield jwt.create_jwt(payload,key);
    })().then((value)=>
    {
        res.json({
            status : constant.responseFlags.SUCCESSFULLY_LOGGED_IN,
            message: constant.responseMessages.LOGIN_SUCCESSFULLY,
            data   : {token : value}
        })
    },(error)=>
    {
        res.json({
            status : constant.responseFlags.INSUFFICIENT_DATA,
            message: constant.responseMessages.TOKEN_NOT_GENERATED,
            data   : {error}
        });
    });
};

module.exports =
{
    newUser     : newUser,
    putUser     : putUser,
    sendMail    : sendMail,
    getUser     : getUser,
    checkUser   : checkUser,
    createToken : createToken
}