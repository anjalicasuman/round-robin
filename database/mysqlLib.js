'use strict';

const mysql   = require('mysql');
const util    = require('util');

var connection     = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'Password@123',
    database : 'assign_driver'
});


connection.connect(function(err) {
    if (err) throw err;
    console.log("connected to database...")
});

function distance(point11,point12,point21,point22)
{
    return new Promise((resolve,reject)=>
    {
        connection.query("select ST_Distance_Sphere( "+
                " point(?, ?), "+
                " point(?, ?) ) * .001",
                [point11,point12,point21,point22],(error,result)=>
                {
                    if(error)
                        reject(error);
                    else 
                        resolve(result);
                });
    })
}


module.exports = 
{
    connection : connection,
    distance   : distance 
}
