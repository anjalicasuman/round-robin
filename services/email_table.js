const sql =  require('../database/mysqlLib').connection;

function enter_values(task_id,driver_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("INSERT INTO email_table SET task_id =?,driver_id =?",[task_id,driver_id],(err,res)=>
        {
            if(err)
                reject(err);
            else 
                resolve(res);
        });
    });
}

function up_values(task_id,driver_id,flag)
{
    return new Promise((resolve, reject)=>
    {
        sql.query("UPDATE email_table SET sent = 1, response =? WHERE task_id =? && driver_id =?",[flag,task_id,driver_id],(err,res)=>
        {
            if(err)
                reject(err);
            else    
                resolve(err);
        })
    })
}

function time_up(task_id,driver_id)
{
    return new Promise((resolve, reject)=>
    {
        sql.query("UPDATE email_table SET time_up =1 WHERE task_id =? && driver_id =?",[task_id,driver_id],(err,res)=>
        {
            if(err)
                reject(err);
            else    
                resolve(err);
        })
    })
}

function check_values(task_id,driver_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("SELECT sent,response FROM email_table WHERE task_id =? AND driver_id =?",[task_id,driver_id],(err,res)=>
        {
            if(err)
                reject(err);
            else 
                resolve(res);
        });
    });
}

function get_time(task_id,driver_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("SELECT time_up FROM email_table WHERE task_id =? AND driver_id =?",[task_id,driver_id],(err,res)=>
        {
            if(err)
                reject(err);
            else 
                resolve(res);
        });
    });
}
module.exports =
{
    enter_values    :   enter_values,
    up_values       :   up_values,
    check_values    :   check_values,
    time_up         :   time_up,
    get_time        :   get_time
}