const sql   = require('../database/mysqlLib').connection;

function create_task(task)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("INSERT INTO task SET ?",task,(err, res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        })
    })
}

function get_task(task_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("SELECT from_long, from_lat FROM task WHERE task_id =?",task_id,(err, res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        })
    })
}

module.exports =
{
    create_task : create_task,
    get_task    : get_task
}