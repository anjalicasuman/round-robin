const sql = require('../database/mysqlLib').connection;

function enter_user(value)
{
    return new Promise((resolve,reject)=>
    {
        sql.query('INSERT INTO user SET ?',value,(err,res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        });
    });
}

function get_user(email,id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query('SELECT * FROM user WHERE email =? OR user_id =? ',[email,id],(err,res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        });
    });
}

function up_pass(password,user_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("UPDATE user SET password = ? WHERE user_id =?",[password,user_id],(err,res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        });
    });
}

module.exports =
{
    enter_user : enter_user,
    get_user   : get_user,
    up_pass    : up_pass  
}