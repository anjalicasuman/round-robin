const sql   = require('../database/mysqlLib').connection;

function enter_driver(value)
{
    return new Promise((resolve,reject)=>
    {
        sql.query('INSERT INTO driver SET ?',value,(err,res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        });
    });
}

function get_driver(email,id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query('SELECT * FROM driver WHERE email =? OR user_id =? ',[email,id],(err,res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        });
    });
}

function up_pass(password,user_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("UPDATE driver SET password = ? WHERE driver_id =?",[password,user_id],(err,res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        });
    });
}

function get_coord()
{
    return new Promise((resolve,reject)=>
    {
        sql.query("SELECT driver_id,email,current_long, current_lat FROM driver WHERE status = 0",(err, res)=>
        {
            if(err)
                reject(err);
            else
                resolve(res);
        })
    })
}

function change_status(driver_id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query("UPDATE task,driver SET task.status = 1,driver.status = 0, task.end_time = NOW() "+
        "WHERE driver.driver_id = ? AND task.driver_id = ? AND task.status = 2",[driver_id,driver_id],(err,value)=>
        {
            if(err)
                reject(err);
            else
                resolve(value);
        })
    })
}

module.exports =
{
    enter_driver    : enter_driver,
    get_driver      : get_driver,
    up_pass         : up_pass,
    get_coord       : get_coord,
    change_status   : change_status
}