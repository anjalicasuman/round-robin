const usermodel = function(userinfo)
{
    this.first_name    = userinfo.first_name;
    this.last_name     = userinfo.last_name;
    this.email         = userinfo.email;
    this.password      = userinfo.password;
    this.DOB           = userinfo.DOB;
    this.current_long  = userinfo.current_long
    this.current_lat   = userinfo.current_lat
};

module.exports = usermodel;