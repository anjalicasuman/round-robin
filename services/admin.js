const sql      = require('../database/mysqlLib').connection;

function get_admin(email,id)
{
    return new Promise((resolve,reject)=>
    {
        sql.query('SELECT admin_id,password FROM admin WHERE email =? OR admin_id =? ',[email,id],(err,res)=>
        {
            if(err)
            {    
                reject(err);
            }
            else
            {   
                resolve(res);
            }
        });
    });
}

module.exports =
{
    get_admin : get_admin
}