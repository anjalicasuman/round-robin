const express           = require('express');
const passport          = require('passport');
const bodyParser        = require('body-parser');
const user              = require('./user/index');
const admin             = require('./admin/index');
const driver            = require('./driver/index');
const session           = require('express-session');
const passportSetup     = require('./utilities/passport');
const swaggerUi         = require('swagger-ui-express'); 
const swaggerDocument   = require('./swagger.json');
const uuid              = require('uuid')

const app     = express();

const port = 3000 || process.env.PORT;

app.set('trust proxy', 1) // trust first proxy
app.use(session({
  genid: (req) => {
    return uuid();
  },
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/admin',admin);
app.use('/driver',driver);
app.use('/user',user);

app.listen(port,async function()
{
    console.log('Server started at port: ',port);
});