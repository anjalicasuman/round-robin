//Constants for messages
exports.responseMessages = {
    "ACTION_COMPLETE"                   : "ACTION_COMPLETE",
    "INVALID_ACCESS_TOKEN"              : "INVALID_ACCESS_TOKEN",
    "INVALID_USERNAME"                  : "INVALID_USERNAME",
    "INVALID_PASSWORD"                  : "INVALID_PASSWORD",
    "INVALID_EMAIL_ID"                  : "INVALID_EMAIL_ID",
    "INVALID_ACCESS"                    : "INVALID_ACCESS",
    "INSUFFICIENT_DATA"                 : "INSUFFICIENT_DATA",
    "SUCESSFULLY_SIGNED_UP"             : "SUCESSFULLY_SIGNED_UP",
    "LOGIN_SUCCESSFULLY"                : "LOGIN_SUCCESSFULLY",
    "OPERATION_UNSUCESSFUL"             : "OPERATION_UNSUCESSFUL",
    "PASSWORD_ERROR"                    : "PASSWORD_ERROR",
    "SHOW_ERROR_MESSAGE"                : "SHOW_ERROR_MESSAGE",
    "TOKEN_NOT_GENERATED"               : "TOKEN_NOT_GENERATED"
};

//Constants for status
exports.responseFlags = {
    INVALID_ACCESS_TOKEN        : 101,
    SUCCESSFULLY_SIGNED_UP      : 200,
    SUCCESSFULLY_LOGGED_IN      : 200,
    OPERATION_SUCESSFUL         : 200,
    ACTION_COMPLETE             : 200,
    INVALID_USERNAME            : 201,
    INVALID_EMAIL_ID            : 201,
    INVALID_ACCESS              : 201,
    INVALID_PASSWORD            : 201,
    INVALID_USERNAME            : 201,
    OPERATION_UNSUCESSFUL       : 201,
    WRONG_PASSWORD              : 201,
    ALREADY_EXIST               : 400,
    INSUFFICIENT_DATA           : 400,
    FORBIDDEN                   : 403,
    NOT_FOUND                   : 404,
};