const Router     = require('express').Router();
const validation = require('./validator/validator');
const controller = require('./controller/controller');

Router.post('/login',validation.getDriver, validation.checkDriver, validation.createToken);
Router.put('/update',controller.verifyToken, controller.updateDriver);
Router.post('/job_status', controller.verifyToken, controller.completeTask);

module.exports = Router;