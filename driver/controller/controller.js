const Promise    = require('bluebird');
const Joi        = require('joi');
const schema     = require('../../utilities/joiSchema');
const bcrypt     = require('../../utilities/bcryptHash');
const sql        = require('../../services/driver');
const constant   = require('../../properties/constants');
const jwt        = require('../../utilities/jwtToken');

function verifyToken(req,res,next)
{
    const bearerHeader =  req.headers['authorization']; //get auth header value
    if(typeof bearerHeader !== 'undefined')            // check if auth undefined
    {
        
            const key = 'secretdriverkey';
            const bearer = bearerHeader.split(' ');
            const token = bearer[1];
            Joi.validate({access_token :token}, schema.Token, function (err, value)  //validating the entered information
            {
                if(err)
                {
                    res.json({                               // providing error message in json format
                    message: 'Insufficient information was supplied. Please check and try again.',
                    status: constant.responseFlags.INSUFFICIENT_DATA,
                    data: err.details[0].message.replace(/["]/ig, ''),
                    });
                }
                else
                {
                    Promise.coroutine(function*()
                    {
                        return yield jwt.check_jwt(token,key);
                    })().then((result)=>
                    {
                        req.id = result.id;
                        next();
                    },(error)=>
                    {
                        res.json({
                            //forbidden
                            status  :  constant.responseFlags.INVALID_ACCESS,
                            message :  constant.responseMessages.INVALID_ACCESS_TOKEN,
                            data    :  {error}
                        });
                    });
                }
            });         
    }
    else{
        //forbidden
        res.json({
            status  :  constant.responseFlags.FORBIDDEN,
            message :  constant.responseMessages.INVALID_ACCESS,
            data    : {}
        });
    }
}
function updateDriver(req,res)
{
    let user =
    {
        user_id  : req.id,
        password : req.body.password
    };
    Promise.coroutine(function* ()
    {
        let value = yield sql.get_driver(null,user.user_id);
        user.hashpass = value[0].password;
        let check = yield bcrypt.check_pass(user.password,user.hashpass);
        if(check == true)
        {
            user.new_pass = req.body.new_password;
            let hashpass = yield bcrypt.encode_pass(user.new_pass);
            let value = yield sql.up_pass(hashpass,user.user_id);
            res.json({                               
                message: constant.responseMessages.ACTION_COMPLETE,
                status : constant.responseFlags.ACTION_COMPLETE,
                data   : value
            });        
        } 
        else
        {
            res.json({
                message: constant.responseMessages.INVALID_PASSWORD,
                status : constant.responseFlags.INVALID_PASSWORD,
                data   : {}
            });
        }
    })().catch((error)=>
    {
        res.json({
            //forbidden
            status  :  constant.responseFlags.INVALID_ACCESS,
            message :  constant.responseMessages.OPERATION_UNSUCESSFUL,
            data    :  {error}
        });
    });
}

function completeTask(req,res)
{
    let driver_id =req.id;
    Promise.coroutine(function*()
    {
        return yield sql.change_status(driver_id);
    })().then((value)=>
    {
        if(value.changedRows == 0)
        {
            let msg = "THERE IS NO ONGOING BOOKING FOR THIS DRIVER";
            res.json({
                //forbidden
                status  :  constant.responseFlags.INVALID_ACCESS,
                message :  msg,
                data    :  {}
            });
        }
        else
        {    
            res.json({                               
                message: constant.responseMessages.ACTION_COMPLETE,
                status : constant.responseFlags.ACTION_COMPLETE,
                data   : value
            }); 
        }
    },(error)=>
    {
        res.json({
            status  :  constant.responseFlags.OPERATION_UNSUCESSFUL,
            message :  constant.responseMessages.OPERATION_UNSUCESSFUL,
            data    :  {error}
        });
    });
}

module.exports =
{
    verifyToken   : verifyToken,
    updateDriver  : updateDriver,
    completeTask  : completeTask
}