const Promise    = require('bluebird');
const Joi        = require('joi');
const schema     = require('../../utilities/joiSchema');
const jwt        = require('../../utilities/jwtToken');
const bcrypt     = require('../../utilities/bcryptHash');
const sql        = require('../../services/driver')
const constant   = require('../../properties/constants');

function getDriver(req,res,next)
{
    let user =
    {
        email : req.body.email,
        password : req.body.password
    };
    Joi.validate(user, schema.Schema, (err, value)=>  //validating the entered information
    {
        if(err)
        {
            res.json({                               // providing error message in json format
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : err.details[0].message.replace(/["]/ig, '')
            });
        }
        else
        {    
            Promise.coroutine(function* ()
            {
                let value = yield sql.get_driver(user.email,null);
                if(value[0] == undefined)
                {
                    res.json({
                        message: constant.responseMessages.INVALID_USERNAME,
                        status : constant.responseFlags.INVALID_USERNAME,
                        data   : {}
                    });
                }
                else
                {   
                    user.hashpass = value[0].password;
                    user.id       = value[0].driver_id;
                    req.user = user;
                    next();
                }
            })().catch((error)=>
            {
                res.json({                               
                    message: constant.responseMessages.INSUFFICIENT_DATA,
                    status : constant.responseFlags.INSUFFICIENT_DATA,
                    data   : {error}
                });
            });
        }
    });
}

function checkDriver(req,res,next)
{
    user = req.user; 
    Promise.coroutine(function*()
    {
        let value = yield bcrypt.check_pass(user.password,user.hashpass);
        if(value == true)
        {
            req.payload = {
                id : user.id
            };
            next();
        } 
        else
        {
            res.json({
                message: constant.responseMessages.INVALID_PASSWORD,
                status : constant.responseFlags.INVALID_PASSWORD,
                data   : {}
            });
        } 
    })().catch((error)=>
    {
        res.json({                               
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : {error}
        });
    });       
}

function createToken(req,res) 
{
    let payload = req.payload,
        key = 'secretdriverkey';
    Promise.coroutine(function*()
    {
        return yield jwt.create_jwt(payload,key);
    })().then((value)=>
    {
        res.json({
            status : constant.responseFlags.SUCCESSFULLY_LOGGED_IN,
            message: constant.responseMessages.LOGIN_SUCCESSFULLY,
            data   : {token : value}
        })
    },(error)=>
    {
        res.json({
            status : constant.responseFlags.INSUFFICIENT_DATA,
            message: constant.responseMessages.TOKEN_NOT_GENERATED,
            data   : {error}
        });
    });
};

module.exports = 
{
    getDriver     : getDriver,
    checkDriver   : checkDriver,
    createToken   : createToken
}