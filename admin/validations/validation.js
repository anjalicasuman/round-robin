const Promise    = require('bluebird');
const Joi        = require('joi');
const schema     = require('../../utilities/joiSchema');
const jwt        = require('../../utilities/jwtToken');
const bcrypt     = require('../../utilities/bcryptHash');
const sql        = require('../../services/admin')
const constant   = require('../../properties/constants');

function getAdmin(req,res,next)
{
    let admin =
    {
        email : req.body.email,
        password : req.body.password
    };
    Joi.validate(admin, schema.Schema, (err, value)=>  //validating the entered information
    {
        if(err)
        {
            res.json({                               // providing error message in json format
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : err.details[0].message.replace(/["]/ig, '')
            });
        }
        else
        {   
            Promise.coroutine(function* ()
            {
                let value = yield sql.get_admin(admin.email,null);
                if(value[0] == undefined)
                {
                    res.json({
                        message: constant.responseMessages.INVALID_USERNAME,
                        status : constant.responseFlags.INVALID_USERNAME,
                        data   : {}
                    });
                }
                else
                {   
                    admin.hashpass = value[0].password;
                    admin.id       = value[0].admin_id;
                    req.admin = admin;
                    next();
                }
            })().catch((error)=>
            {
                res.json({                               
                    message: constant.responseMessages.INSUFFICIENT_DATA,
                    status : constant.responseFlags.INSUFFICIENT_DATA,
                    data   : {error}
                });
            });
        }
    });
}

function checkAdmin(req,res,next)
{
    admin = req.admin; 
    Promise.coroutine(function*()
    {
        let value = yield bcrypt.check_pass(admin.password,admin.hashpass);
        if(value == true)
        {
            req.payload = {
                id : admin.id
            };
            next();
        } 
        else
        {
            res.json({
                message: constant.responseMessages.INVALID_PASSWORD,
                status : constant.responseFlags.INVALID_PASSWORD,
                data   : {}
            });
        } 
    })().catch((error)=>
    {
        res.json({                               
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : {error}
        });
    });       
}

function createToken(req,res) 
{
    let payload = req.payload,
        key = 'secretadminkey';
    Promise.coroutine(function*()
    {
        return yield jwt.create_jwt(payload,key);
    })().then((value)=>
    {
        res.json({
            status : constant.responseFlags.SUCCESSFULLY_LOGGED_IN,
            message: constant.responseMessages.LOGIN_SUCCESSFULLY,
            data   : {token : value}
        })
    },(error)=>
    {
        res.json({
            status : constant.responseFlags.INSUFFICIENT_DATA,
            message: constant.responseMessages.TOKEN_NOT_GENERATED,
            data   : {error}
        });
    });
};

module.exports = 
{
    getAdmin    : getAdmin,
    checkAdmin  : checkAdmin,
    createToken : createToken
}