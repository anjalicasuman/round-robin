const Promise    = require('bluebird');
const Joi        = require('joi');
const schema     = require('../../utilities/joiSchema');
const bcrypt     = require('../../utilities/bcryptHash');
const sql        = require('../../services/driver');
const constant   = require('../../properties/constants');
const jwt        = require('../../utilities/jwtToken');
const usermodel  = require('../../services/usermodel') 

function verifyToken(req,res,next)
{
    const bearerHeader =  req.headers['authorization']; //get auth header value
    if(typeof bearerHeader !== 'undefined')            // check if auth undefined
    {
        
            const key = 'secretadminkey';
            const bearer = bearerHeader.split(' ');
            const token = bearer[1];
            Joi.validate({access_token :token}, schema.Token, function (err, value)  //validating the entered information
            {
                if(err)
                {
                    res.json({                               // providing error message in json format
                    message: 'Insufficient information was supplied. Please check and try again.',
                    status: constant.responseFlags.INSUFFICIENT_DATA,
                    data: err.details[0].message.replace(/["]/ig, ''),
                    });
                }
                else
                {
                    Promise.coroutine(function*()
                    {
                        return yield jwt.check_jwt(token,key);
                    })().then((result)=>
                    {
                        req.id = result.id;
                        next();
                    },(error)=>
                    {
                        res.json({
                            //forbidden
                            status  :  constant.responseFlags.INVALID_ACCESS,
                            message :  constant.responseMessages.INVALID_ACCESS_TOKEN,
                            data    :  {error}
                        });
                    });
                }
            });         
    }
    else{
        //forbidden
        res.json({
            status  :  constant.responseFlags.FORBIDDEN,
            message :  constant.responseMessages.INVALID_ACCESS,
            data    : {}
        });
    }
}

function getDriver(req,res,next)
{
    let driver = new usermodel(req.body);
    Joi.validate(driver, schema.Schema, function (err, value)  //validating the entered information
    {
        if(err)
        {
            res.json({                               // providing error message in json format
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : err.details[0].message.replace(/["]/ig, '')
            });
        }
        else
        {   
            req.driver = driver;
            next();
        }
    });
}

function putDriver(req,res)
{
    let driver = req.driver;
    Promise.coroutine(function*()
    {
        driver.password = yield bcrypt.encode_pass(driver.password);
        return yield sql.enter_driver(driver);
    })().then((value)=>
    {
        res.json({
            message: constant.responseMessages.SUCESSFULLY_SIGNED_UP,
            status : constant.responseFlags.SUCCESSFUL_SIGNED_UP,
            data: {
                Firstname     : driver.first_name,
                Lastname      : driver.last_name,
                Email         : driver.email,
                DOB           : driver.DOB,
                Longitude     : driver.current_long,
                Latitude      : driver.current_lat    
            }
        });
    },(error)=>
    {
        res.json({                               
            message: constant.responseMessages.INSUFFICIENT_DATA,
            status : constant.responseFlags.INSUFFICIENT_DATA,
            data   : {error}
        });
    });
}

module.exports =
{
    verifyToken : verifyToken,
    getDriver   : getDriver,
    putDriver   : putDriver
}