const Router     = require('express').Router();
const validation = require('./validations/validation');
const controller = require('./controller/controller');

Router.post('/login', validation.getAdmin, validation.checkAdmin, validation.createToken);
Router.post('/register_driver',controller.verifyToken, controller.getDriver, controller.putDriver);

module.exports = Router;