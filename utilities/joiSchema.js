const Joi = require('joi');

const Schema = Joi.object().keys({
    first_name    : Joi.string().regex(/^[a-zA-Z]{3,24}$/),
    last_name     : Joi.string().regex(/^[a-zA-Z]{3,24}$/),
    email         : Joi.string().email({ minDomainAtoms: 2 }).required(),  
    password      : Joi.string().regex(/^[a-zA-Z0-9@!#$%^&*]{3,20}$/).required(),
    DOB           : Joi.date(),
    current_long  : Joi.number(),
    current_lat   : Joi.number()
});

const Token = Joi.object().keys({
    access_token: Joi.string().min(150).required()
});

module.exports = 
{
    Schema : Schema,
    Token  : Token
}