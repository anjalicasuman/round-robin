const bcrypt = require('bcrypt');

exports.encode_pass = (password) =>
{
    return new Promise((resolve,reject)=>
    {
        bcrypt.hash(password, 10).then((hash)=>
        {
            resolve(hash);
        }).catch((error)=>
        {
            reject(error);
        });
    });
};

exports.check_pass = (password, hashpass) =>
{
    return new Promise((resolve,reject)=>{
        bcrypt.compare(password, hashpass).then((check)=>
        {
            resolve(check);
        }).catch((error)=>
        {
            reject(error);
        });
    });
};