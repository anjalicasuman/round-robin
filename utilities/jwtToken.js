'use strict';

const jwt = require('jsonwebtoken');

exports.create_jwt = (payload,key) =>
{
    return new Promise((resolve,reject) =>
    {   jwt.sign(payload,key,{algorithm : 'HS512'},(err,token) =>
        {
            if(err)
                reject(err);
            else
                resolve(token);
        });
    });
};

exports.check_jwt = (token,key) =>
{
    return new Promise((resolve,reject) =>
    {    jwt.verify(token, key,{algorithm : 'HS512'},(err, decoded) => 
        {
            if (err)
                reject(err);
            else
                resolve(decoded); 
        });
    }); 
};