const configAuth       = require('../config/config');
const passport         = require('passport');
const GoogleStrategy   = require('passport-google-oauth20').Strategy;
const sql              = require('../database/mysqlLib');
const Promise          = require('bluebird'); 

passport.use(new GoogleStrategy({
    clientID: configAuth.GoogleStrategy.clientID,
    clientSecret: configAuth.GoogleStrategy.clientSecret,
    callbackURL: configAuth.GoogleStrategy.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {
      console.log(profile);
    Promise.coroutine(function* (){
        let value = yield sql.get_external(profile.id,profile.provider);
        if(value[0] != undefined)
        {
            return done(null,value[0]);
        }
        else
        {
            let name = profile.displayName.split(' ');
            let user =
            {
                first_name  : name[0],
                last_name   : name[1],
                email       : profile.emails[0].value,
                password    : profile.id,
                token       : accessToken,
                external_id : profile.id,
                provider    : profile.provider
            }
            yield sql.in_user(user);
            return done(null,user);
        }
    })().catch((error)=>
    {
        done(error);
    });
  }
));