const request   = require('request');
const Promise   = require('bluebird');
const task      = require('../services/task');
const sql       = require('../database/mysqlLib');
const driver    = require('../services/driver');

let dist_arr =[];

function fetch_distance(task_id)
{
    return Promise.coroutine(function* ()
    {
        let assigntask = yield task.get_task(task_id);
        let freedriver   = yield driver.get_coord();
        for(let i =0; i < freedriver.length; i++ ){
            let value = yield sql.distance(assigntask[0].from_long,assigntask[0].from_lat,freedriver[i].current_long,freedriver[i].current_lat);
            for (var key in value[0]) {
                if (value[0].hasOwnProperty(key)) {
                    dist_arr[i] = {
                        driver_id : freedriver[i].driver_id,
                        email     : freedriver[i].email,
                        distance  : value[0][key]
                    };
                }
              } 
        }
        if(assigntask[0] == undefined || freedriver[0] == undefined)
        {
            throw new Error("Values could not be fetched.");
        }
        else
        {
            dist_arr.sort(function compare(a,b) {
                if (a.distance < b.distance)
                  return -1;
                if (a.distance > b.distance)
                  return 1;
                return 0;
            });
            return Promise.resolve(dist_arr);
        }
    })();
    
}


module.exports =
{
    fetch_distance  : fetch_distance
}